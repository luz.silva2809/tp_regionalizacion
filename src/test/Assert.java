package test;

import java.util.Set;

import negocio.Grafo;

import static org.junit.Assert.*;

public class Assert 
{
	
	//devuelve una assertion true si se cumple que ambos conjuntos sean iguales
	public static void iguales(String[] esperados, Set<String> obtenidos) 
	{
		assertEquals(esperados.length, obtenidos.size());
		for (int i = 0 ; i < esperados.length; i++) 
		{
			assertTrue(obtenidos.contains(esperados[i]));
		}
	}

	
	// devuelve una assertion true si se cumple que ambos grafos son iguales
	public static void grafosIguales(Grafo esperado, Grafo obtenido) 
	{
		assertEquals(esperado.tama�o(), obtenido.tama�o());
		
		for (String ciudad: obtenido.getCiudades()) 
		{
			for(String  vecina: obtenido.vecinosDe(ciudad)) {
				assertTrue(esperado.existeArista(ciudad, vecina));
			}
		}
	}
}

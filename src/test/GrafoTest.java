package test;

import static org.junit.Assert.*;

import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import negocio.Grafo;


public class GrafoTest 
{
		Grafo grafo;
		String provincia;
		String provincia2;

		
		@Before
		public void iniciarGrafo()
		{
			grafo = new Grafo();
			provincia = "Bs. As";
			provincia2 = "Cordoba";
			grafo.agregarVertice(provincia);
			grafo.agregarVertice(provincia2);
		}
		
		
		
		//agregarArista
		
		@Test (expected = IllegalArgumentException.class)
		public void agregarLoopTest()
		{		
			grafo.agregarArista(provincia, provincia , 5);			
		}
		
		
		@Test 
		public void agregarAristaTest()
		{
			grafo.agregarArista(provincia, provincia2 , 5);
			
			assertTrue(grafo.existeArista(provincia, provincia2));
		}
		
		
		@Test
		public void agregarAristaOpuestaTest()
		{			
			grafo.agregarArista(provincia, provincia2 , 5);
			
			assertTrue(grafo.existeArista(provincia2,provincia));
		}
		
		
		@Test 
		public void agregarAristaDosVecesTest()
		{
			grafo.agregarArista(provincia, provincia2 , 5);
			grafo.agregarArista(provincia, provincia2 , 5);
			
			assertTrue(grafo.existeArista(provincia,provincia2));
		}
		
		
		@Test
		public void reemplazarAristaTest()
		{
			grafo.agregarArista(provincia, provincia2, 5);
			grafo.agregarArista(provincia, provincia2 , 6);
			
			assertFalse(5 == grafo.getSimilaridad(provincia, provincia2));
		}
		
		
		
		//AgregarVertice

		@Test
		public void AgregarVerticeTest()
		{
			assertTrue(grafo.existeVertice(provincia));
		}
		
		
		@Test
		public void AgregarVerticeDosVecesTest()
		{	
			grafo.agregarVertice(provincia);
			
			assertEquals(2, grafo.tama�o());
		}
		
		
		
		//eliminarArista
		
		 @Test(expected = IllegalArgumentException.class)
		 public void eliminarAristaInexistenteTest()
		 {
			 String provincia3="chaco";
			 grafo.agregarVertice(provincia3);
			 grafo.eliminarArista(provincia, provincia3);
		 }
		 
		 
		 @Test (expected = IllegalArgumentException.class)
		 public void eliminarAristaDosVecesTest()
		 {
			 grafo.eliminarArista(provincia, provincia2);
			 grafo.eliminarArista(provincia, provincia2);
		 }

		 
		 @Test 
		 public void eliminarAristaExistenteTest()
		 {
			 grafo.agregarArista(provincia, provincia2, 3);
			 grafo.eliminarArista(provincia, provincia2);
			 
			 assertFalse(grafo.existeArista(provincia, provincia2));
		 }
		 
		 
		 @Test
		 public void eliminarAristaInversaTest()
		 {
			 grafo.agregarArista(provincia, provincia2, 5);
			 grafo.eliminarArista(provincia2, provincia);
			 assertFalse(grafo.existeArista(provincia, provincia2));
		 }
		 
		 
		 
		 //eliminarVertice
		 
		 @Test (expected = NullPointerException.class)
		 public void eliminarVerticeInexistenteTest()
		 {
			 String provincia3="chaco";
			 
			 grafo.eliminarVertice(provincia3);
		 }
		 
		 
		 @Test (expected = NullPointerException.class)
		 public void eliminarVerticeDosVecesTest()
		 {
			grafo.eliminarVertice(provincia);
			grafo.eliminarVertice(provincia);
		 }
		 
		 
		 @Test
		 public void eliminarVerticeTest()
		 {
			 grafo.eliminarVertice(provincia);
			 assertEquals(1,grafo.tama�o());
		 }
		 
		 
		 @Test
		 public void comprobarRelacionVerticeEliminadoTest()
		 {
			 grafo.agregarArista(provincia, provincia2, 10);
			 grafo.eliminarVertice(provincia);
			 
			 assertFalse(grafo.existeArista(provincia2, provincia));
		 }
		 
		 
		 
		 //getCiudades
		 
		 @Test
		 public void grafoVacioTest()
		 {
			 Grafo g=new Grafo();
			 
			 assertEquals(0, g.getCiudades().size());
		 }
		 
		 @Test
		 public void ciudadesTest()
		 {
			 String [] esperados= {provincia, provincia2};
			 Set<String> obtenidos= grafo.getCiudades();
			
			 Assert.iguales(esperados,obtenidos);
		 }
		 
		 
		 
		 //regionalizar
		 
		 @Test(expected=IllegalArgumentException.class)
		 public void cantidadDeRegionesExcedida()
		 {
			 grafo.agregarArista(provincia2, provincia, 2);
			 grafo.regionalizar(3);
		 }
		 
		 
		 @Test
		 public void UnaAristaUnaRegionTest()
		 {
			 grafo.agregarArista(provincia2, provincia, 4);
			 grafo.regionalizar(1);
			 
			 assertTrue(grafo.existeArista(provincia2, provincia));
		 }
		 
		 
		 @Test
		 public void UnaAristaDosRegionesTest()
		 {
			 grafo.agregarArista(provincia2, provincia, 4);
			 grafo.regionalizar(2);
			 
			 assertFalse(grafo.existeArista(provincia2, provincia));
		 }
		 
		 
		 @Test
		 public void regionalizarMapaTest()
		 {
			 String provincia3="Salta";
			 String provincia4="Jujuy";
			 String provincia5="Santiago del Estero";
		 
			 grafo.agregarVertice(provincia4);
			 grafo.agregarVertice(provincia3);
			 grafo.agregarVertice(provincia5);
			 
			 grafo.agregarArista(provincia, provincia2, 1);
			 grafo.agregarArista(provincia, provincia5, 2);
			 grafo.agregarArista(provincia2, provincia3, 2);
			 grafo.agregarArista(provincia2, provincia4, 5);
			 
			 grafo.regionalizar(2);
			 assertFalse(grafo.existeArista(provincia2, provincia4));
		 }

}

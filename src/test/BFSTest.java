package test;

import static org.junit.Assert.*;

import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import negocio.Grafo;
import negocio.BFS;

public class BFSTest {
	
	Grafo grafo;
	String provincia;
	String provincia2;
	String provincia3;
	String provincia4;
	String provincia5;

	
	@Before
	public void iniciarGrafo()
	{
		grafo = new Grafo();
		
		provincia = "Bs. As";
		provincia2 = "Cordoba";
		provincia3 = "Santa Fe";
		provincia4 = "San Juan";
		provincia5 = "Tierra del Fuego";
		
		grafo.agregarVertice(provincia);
		grafo.agregarVertice(provincia2);
		grafo.agregarVertice(provincia3);
		grafo.agregarVertice(provincia4);
		grafo.agregarVertice(provincia5);
	}

	
	
	@Test (expected=NullPointerException.class)
	public void grafoNuloTest() 
	{
		Grafo g = null;
		
		assertFalse(BFS.esConexo(g));
	}
	
	
	
	@Test
	public void esConexoTest() 
	{
		grafo.agregarArista(provincia, provincia3, 1);
		grafo.agregarArista(provincia, provincia2, 1);
		grafo.agregarArista(provincia3, provincia4, 1);
		grafo.agregarArista(provincia3, provincia2, 1);
		grafo.agregarArista(provincia2, provincia5, 1);
		
		assertTrue(BFS.esConexo(grafo));
	}
	
	
	@Test
	public void noConexoTest() 
	{
		grafo.agregarArista(provincia4, provincia5, 1);
		grafo.agregarArista(provincia, provincia3, 1);
		grafo.agregarArista(provincia, provincia2, 1);
		grafo.agregarArista(provincia3, provincia2, 1);
		
		assertFalse(BFS.esConexo(grafo));
	}
	
	
	
	@Test 
	public void grafoVacioTest() 
	{
		Grafo g = new Grafo();
		
		assertTrue(BFS.esConexo(g));
	}
	
	
	@Test
	public void alcanzablesTest()
	{
		grafo.agregarArista(provincia, provincia3, 1);
		grafo.agregarArista(provincia3, provincia2, 1);
		grafo.agregarArista(provincia5, provincia2, 1);
		grafo.agregarArista(provincia5, provincia4, 1);
		
		Set<String> obtenidos=BFS.alcanzables(grafo, provincia);
		String[] esperados = {provincia,provincia2,provincia3,provincia4,provincia5};
		
		Assert.iguales(esperados,obtenidos);
	}
	
	
	@Test
	public void unaSolaAristaTest()
	{
		Grafo g=new Grafo();
		String c1="La pampa";
		String c2="Entre Rios";
		g.agregarVertice(c2);
		g.agregarVertice(c1);
		g.agregarArista(c1, c2, 10);
		assertTrue(BFS.esConexo(g));
	}
	
}

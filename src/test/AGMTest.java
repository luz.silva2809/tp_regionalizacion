package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import negocio.AGM;
import negocio.Grafo;

public class AGMTest 
{
	Grafo grafo;
	String provincia;
	String provincia2;
	String provincia3;
	String provincia4;
	String provincia5;

	@Before
	public void iniciarGrafo()
	{
		grafo = new Grafo();
		
		provincia = "Bs. As";
		provincia2 = "Cordoba";
		provincia3 = "Santa Fe";
		provincia4 = "San Juan";
		provincia5 = "Tierra del Fuego";
		
		grafo.agregarVertice(provincia);
		grafo.agregarVertice(provincia2);
		grafo.agregarVertice(provincia3);
		grafo.agregarVertice(provincia4);
		grafo.agregarVertice(provincia5);
	}
	
	
	@Test(expected= IllegalArgumentException.class)
	public void grafoDisconexoTest()
	{
		grafo.agregarArista(provincia4, provincia5, 1);
		grafo.agregarArista(provincia, provincia3, 1);
		grafo.agregarArista(provincia, provincia2, 1);
		grafo.agregarArista(provincia3, provincia2, 1);
		
		AGM.generar(grafo);
	
	}
	
	
	@Test
	public void unaSolaAristaTest()
	{
		Grafo grafo1=new Grafo();
		grafo1.agregarVertice(provincia);
		grafo1.agregarVertice(provincia2);
		grafo1.agregarArista(provincia2, provincia, 10);
		
		Grafo grafo2=AGM.generar(grafo1);
		
		Assert.grafosIguales(grafo1, grafo2);
	}
	
	
	@Test
	public void arbolGeneradorMinimoTest()
	{
		grafo.agregarArista(provincia, provincia3,5 );
		grafo.agregarArista(provincia, provincia2, 1);
		grafo.agregarArista(provincia3, provincia4, 6);
		grafo.agregarArista(provincia3, provincia2, 9);
		grafo.agregarArista(provincia2, provincia5, 7);
		grafo.agregarArista(provincia3, provincia5, 9);
		grafo.agregarArista(provincia2, provincia4, 1);
		grafo.agregarArista(provincia, provincia4, 3);
		grafo.agregarArista(provincia, provincia5, 2);
		grafo.agregarArista(provincia4, provincia5, 3);
		
		Grafo esperado=new Grafo();
		esperado.agregarVertice(provincia);
		esperado.agregarVertice(provincia2);
		esperado.agregarVertice(provincia3);
		esperado.agregarVertice(provincia4);
		esperado.agregarVertice(provincia5);
		
		esperado.agregarArista(provincia2, provincia, 1);
		esperado.agregarArista(provincia3, provincia, 5);
		esperado.agregarArista(provincia, provincia5, 2);
		esperado.agregarArista(provincia2, provincia4, 1);
		
		Assert.grafosIguales(esperado, AGM.generar(grafo));
	}
	
	
	@Test 
	public void aristaIncorrectaEnArbolTest()
	{
		grafo.agregarArista(provincia, provincia3,5 );
		grafo.agregarArista(provincia, provincia2, 1);
		grafo.agregarArista(provincia3, provincia4, 6);
		grafo.agregarArista(provincia3, provincia2, 9);
		grafo.agregarArista(provincia2, provincia5, 7);
		grafo.agregarArista(provincia3, provincia5, 9);
		grafo.agregarArista(provincia2, provincia4, 1);
		grafo.agregarArista(provincia, provincia4, 3);
		grafo.agregarArista(provincia, provincia5, 2);
		grafo.agregarArista(provincia4, provincia5, 3);
		
		Grafo arbol=AGM.generar(grafo);
		
		assertFalse(arbol.existeArista(provincia3, provincia2));
	}
	
}

package usuario;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import java.awt.Font;
import java.awt.Rectangle;

import javax.swing.SwingConstants;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import org.openstreetmap.gui.jmapviewer.MapPolygonImpl;
import org.openstreetmap.gui.jmapviewer.interfaces.MapMarker;
import org.openstreetmap.gui.jmapviewer.interfaces.MapPolygon;

import negocio.AGM;
import negocio.BFS;
import negocio.Grafo;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.Component;

public class Ventana {

	private JFrame frame;
	private Grafo mapa;
	private Grafo arbolGeneradorMinimo;
	private JMapViewer vistaDeMapa;
	private ArrayList<MapMarker> listaDeCoordenadas;
	private boolean seAplicoAGM;
	
	
	//muestra en un mapaVisual las ciudades y las aristas entre las ciudades
	public void actualizarMapaVisual(JMapViewer mapaVisual, Grafo grafo)
	{
		mapaVisual.removeAllMapPolygons();
		mapaVisual.removeAllMapMarkers();
		actualizarVertices(mapaVisual,grafo);
		actualizarAristas(mapaVisual,grafo);
	}

	
	//ubica las aristas en el mapaVisual
	private void actualizarAristas(JMapViewer mapaVisual,Grafo grafo) 
	{
		for (String ciudad : grafo.getCiudades()) 
		{
			Coordinate origen = null;
			Coordinate destino = null;
			for (String vecino : grafo.vecinosDe(ciudad)) 
			{
				for (MapMarker coordenada : listaDeCoordenadas)
				{
					if(ciudad.equals(coordenada.getName()))
					{
						origen = coordenada.getCoordinate();
					}
					if(vecino.equals(coordenada.getName()))
					{
						destino = coordenada.getCoordinate();
					}
				}
				ArrayList<Coordinate> arista = new ArrayList<Coordinate>
				(Arrays.asList(origen, destino, destino));
				
				MapPolygon vistaArista = new MapPolygonImpl(arista);
				vistaArista.getStyle().setColor(Color.BLACK);
				mapaVisual.addMapPolygon(vistaArista);
			}
		}
	}

	
	//ubica las ciudades en el mapaVisual
	private void actualizarVertices(JMapViewer mapaVisual, Grafo grafo) 
	{
		for (String ciudad : grafo.getCiudades()) 
		{
			for (MapMarker coordenada : listaDeCoordenadas) 
			{
				if(ciudad.equals(coordenada.getName()))
				{
					coordenada.getStyle().setBackColor(Color.RED);
					mapaVisual.addMapMarker(coordenada);
				}
			}
		}
		
	}


	
	public static void main(String[] args) 
	{
		EventQueue.invokeLater(new Runnable() 
		{
			public void run() 
			{
				try 
				{
					Ventana window = new Ventana();
					window.frame.setVisible(true);
				} 
				catch (Exception e) 
				{
					e.printStackTrace();
				}
			}
		});
	}


	
	public Ventana() 
	{
		initialize();
	}

	

	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 50, 900, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		
		//Se inicializan variables necesarias
		mapa=new Grafo();
		
		listaDeCoordenadas=new ArrayList<MapMarker>();
		
		seAplicoAGM=false;
		
		String [] provincias=new String []{"Seleccione", "Buenos Aires", "Catamarca", "Chaco", "Chubut", "Cordoba", "Corrientes", 
				"Entre Rios", "Formosa", "Jujuy", "La Pampa", "La Rioja", "Mendoza", "Misiones", "Neuquen",
				"Rio Negro", "Salta", "San Juan", "San Luis", "Santa Cruz", "Santa Fe", "Santiago del Estero",
				"Tierra del Fuego", "Tucuman"};
		
		String [] numeros=new String[] {"Seleccione","0","1","2","3","4","5","6","7","8","9","10"};

		listaDeCoordenadas.add(new MapMarkerDot("Buenos Aires", new Coordinate(-35,-60)));
		listaDeCoordenadas.add(new MapMarkerDot("Catamarca", new Coordinate(-27,-67)));
		listaDeCoordenadas.add(new MapMarkerDot("Chaco", new Coordinate(-27,-60)));
		listaDeCoordenadas.add(new MapMarkerDot("Chubut", new Coordinate(-43,-68)));
		listaDeCoordenadas.add(new MapMarkerDot("Cordoba", new Coordinate(-31,-64)));
		listaDeCoordenadas.add(new MapMarkerDot("Corrientes", new Coordinate(-29,-58)));
		listaDeCoordenadas.add(new MapMarkerDot("Entre Rios", new Coordinate(-32,-59)));
		listaDeCoordenadas.add(new MapMarkerDot("Formosa", new Coordinate(-25,-60)));
		listaDeCoordenadas.add(new MapMarkerDot("Jujuy", new Coordinate(-23,-66)));
		listaDeCoordenadas.add(new MapMarkerDot("La Pampa", new Coordinate(-37,-65)));
		listaDeCoordenadas.add(new MapMarkerDot("La Rioja", new Coordinate(-30,-67)));
		listaDeCoordenadas.add(new MapMarkerDot("Mendoza", new Coordinate(-34,-68)));
		listaDeCoordenadas.add(new MapMarkerDot("Misiones", new Coordinate(-27,-54)));
		listaDeCoordenadas.add(new MapMarkerDot("Neuquen", new Coordinate(-38,-70)));
		listaDeCoordenadas.add(new MapMarkerDot("Rio Negro", new Coordinate(-40,-66)));
		listaDeCoordenadas.add(new MapMarkerDot("Salta", new Coordinate(-25,-65)));
		listaDeCoordenadas.add(new MapMarkerDot("San Juan", new Coordinate(-31,-69)));
		listaDeCoordenadas.add(new MapMarkerDot("San Luis", new Coordinate(-33,-66)));
		listaDeCoordenadas.add(new MapMarkerDot("Santa Cruz", new Coordinate(-48,-70)));
		listaDeCoordenadas.add(new MapMarkerDot("Santa Fe", new Coordinate(-30,-61)));
		listaDeCoordenadas.add(new MapMarkerDot("Santiago del Estero", new Coordinate(-27,-63)));
		listaDeCoordenadas.add(new MapMarkerDot("Tierra del Fuego", new Coordinate(-55,-67)));
		listaDeCoordenadas.add(new MapMarkerDot("Tucuman", new Coordinate(-27,-65)));
		
		
		
		//LABELS
		JLabel ciudad1 = new JLabel("Provincia de origen");
		ciudad1.setHorizontalAlignment(SwingConstants.CENTER);
		ciudad1.setFont(new Font("Arial", Font.PLAIN, 16));
		ciudad1.setBounds(390, 320, 150, 20);
		frame.getContentPane().add(ciudad1);
		
		JLabel ciudad2 = new JLabel("Provincia de destino");
		ciudad2.setHorizontalAlignment(SwingConstants.CENTER);
		ciudad2.setFont(new Font("Arial", Font.PLAIN, 16));
		ciudad2.setBounds(550, 320, 150, 20);
		frame.getContentPane().add(ciudad2);
		
		JLabel distanciaPeso = new JLabel("Similaridad");
		distanciaPeso.setHorizontalAlignment(SwingConstants.CENTER);
		distanciaPeso.setFont(new Font("Arial", Font.PLAIN, 16));
		distanciaPeso.setBounds(710, 320, 100, 20);
		frame.getContentPane().add(distanciaPeso);
		
		JLabel Titulo = new JLabel("Regionalizaci\u00F3n");
		Titulo.setAlignmentX(Component.CENTER_ALIGNMENT);
		Titulo.setHorizontalAlignment(SwingConstants.CENTER);
		Titulo.setFont(new Font("Arial", Font.PLAIN, 27));
		Titulo.setBounds(455, 25, 300, 50);
		frame.getContentPane().add(Titulo);
		
		
		
		
		//MAPA VISUAL
		JPanel panelMapa = new JPanel();
		panelMapa.setRequestFocusEnabled(false);
		panelMapa.setBounds(new Rectangle(25, 25, 350, 500));
		frame.getContentPane().add(panelMapa);
		
		vistaDeMapa = new JMapViewer();
		vistaDeMapa.setBounds(0, 0, 350, 500);
		Coordinate coordenadasCentroArgentina = new Coordinate(-41, -65);
		vistaDeMapa.setDisplayPosition(coordenadasCentroArgentina, 4);
		panelMapa.setLayout(null);
		panelMapa.add(vistaDeMapa);
		
		
		
		
		//SELECCIONADORES
		JComboBox<String> choicePcia1 = new JComboBox <String>();
		choicePcia1.setFont(new Font("Arial", Font.PLAIN, 15));
		choicePcia1.setBounds(390, 345, 150, 21);
		frame.getContentPane().add(choicePcia1);
		
		JComboBox<String> choicePcia2 = new JComboBox<String>();
		choicePcia2.setFont(new Font("Arial", Font.PLAIN, 15));
		choicePcia2.setBounds(550, 345, 150, 21);
		frame.getContentPane().add(choicePcia2);
		
		JComboBox<Object> Similaridad = new JComboBox<Object>();
		Similaridad.setFont(new Font("Arial", Font.PLAIN, 15));
		Similaridad.setBounds(710, 345, 115, 21);
		frame.getContentPane().add(Similaridad);
	
		
		
		choicePcia1.setModel(new DefaultComboBoxModel<String>(provincias));
		choicePcia2.setModel(new DefaultComboBoxModel<String>(provincias));
		
		Similaridad.setModel(new DefaultComboBoxModel<Object>(numeros));
		
	
		
		
		//BOTON AGREGAR
		JButton botonAgregar = new JButton("Agregar");
		botonAgregar.setFont(new Font("Arial", Font.PLAIN, 15));
		botonAgregar.setBounds(440, 400, 100, 30);
		frame.getContentPane().add(botonAgregar);
		botonAgregar.setToolTipText("Agrega una conexion entre provincia de origen y provincia de destino");
		
		
		//accion agregar
		botonAgregar.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				int indiceProvincia1 = choicePcia1.getSelectedIndex();
				int indiceProvincia2 = choicePcia2.getSelectedIndex();
				
				if( indiceProvincia1 != 0 && indiceProvincia2 != 0 
						&& Similaridad.getSelectedIndex() != 0)
				{
					String ciudad1= (String)choicePcia1.getSelectedItem();
					String ciudad2= (String)choicePcia2.getSelectedItem();
					Integer similaridad= Integer.parseInt((String) Similaridad.getSelectedItem());
					
					if(!ciudad1.equals(ciudad2))
					{
						mapa.agregarVertice(ciudad1);
						mapa.agregarVertice(ciudad2);
						mapa.agregarArista(ciudad1, ciudad2, similaridad);
						actualizarMapaVisual(vistaDeMapa, mapa);
					}
					else
					{
						JOptionPane.showMessageDialog(frame, "La provicia de origen no puede ser igual a la provincia de destino");
					}
				}
				else
				{
					if(choicePcia1.getSelectedIndex() == 0 || choicePcia2.getSelectedIndex() == 0)
					{
						JOptionPane.showMessageDialog(frame, "Seleccione una provincia");
					}
					if(Similaridad.getSelectedIndex() == 0)
					{
						JOptionPane.showMessageDialog(frame, "Seleccione un valor num�rico");
					}
				}
			}
		});
		
		
		
		//BOTON DIVIDIR EN REGIONES
		JButton Regionalizar = new JButton("Dividir en regiones");
		Regionalizar.setAlignmentX(Component.CENTER_ALIGNMENT);
		Regionalizar.setFont(new Font("Arial", Font.PLAIN, 18));
		Regionalizar.setBounds(615, 459, 215, 66);
		frame.getContentPane().add(Regionalizar);
		Regionalizar.setToolTipText("Se divide el mapa en la cantidad de regiones que desee");
		
		
		//accion dividir en regiones
		Regionalizar.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e)
			{
				Integer regiones=null;
				
				if(seAplicoAGM == true)
				{
					if(BFS.esConexo(mapa))
					{
						String cantidadRegiones= JOptionPane.showInputDialog
								(frame,"En cuantas regiones desea dividir el mapa","cantidad de regiones",
										JOptionPane.QUESTION_MESSAGE);

						try 
						{
							if(Integer.parseInt(cantidadRegiones) > 0 && Integer.parseInt(cantidadRegiones) <= mapa.tama�o())
							{
								regiones=Integer.parseInt(cantidadRegiones);

								JMapViewer mapaRegionalizado = new JMapViewer();

								Grafo arbolRegionalizado=mapa.clonar();

								arbolRegionalizado.regionalizar(regiones);

								actualizarMapaVisual(mapaRegionalizado, arbolRegionalizado);

								new Regionalizacion(mapaRegionalizado).setVisible(true);

							}
							else 
							{
								JOptionPane.showMessageDialog(frame, "El valor ingresado debe ser mayor a cero y menor o igual a la cantidad de provincias");
							}

						}
						catch(NumberFormatException ex)
						{
							JOptionPane.showMessageDialog(frame, "ingrese valor numerico");
						}
					}
					else
					{
						JOptionPane.showMessageDialog(frame, "El mapa que esta intentando dividir no es conexo, asegurese de que las provincias ingresadas esten conectadas entre si");
					}
				}
				else
				{
					JOptionPane.showMessageDialog(frame, "Debe ingresar al menos una relaci�n entre dos provincias y presionar el bot�n AGM");
				}
				
			}
		});


		
		
		//BOTON ELIMINAR
		JButton botonEliminar = new JButton("Eliminar");
		botonEliminar.setFont(new Font("Arial", Font.PLAIN, 15));
		botonEliminar.setBounds(560, 400, 100, 30);
		frame.getContentPane().add(botonEliminar);
		botonEliminar.setToolTipText("Elimina la conexion entre provincia de origen y provincia de destino");
		
		
		//accion eliminar
		botonEliminar.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				int indiceProvincia1 = choicePcia1.getSelectedIndex();
				int indiceProvincia2 = choicePcia2.getSelectedIndex();
				
				if( indiceProvincia1 != 0 && indiceProvincia2 != 0)
				{
					String ciudad1= (String)choicePcia1.getSelectedItem();
					String ciudad2= (String)choicePcia2.getSelectedItem();
					
					try 
					{
					if(!ciudad1.equals(ciudad2))
					{
						mapa.eliminarArista(ciudad1, ciudad2);
						actualizarMapaVisual(vistaDeMapa, mapa);
					}
					else
					{
						JOptionPane.showMessageDialog(frame, "La provicia de origen no puede ser igual a la provincia de destino");
					}
					}
					catch(IllegalArgumentException aristaInexistente)
					{
						JOptionPane.showMessageDialog(frame,aristaInexistente.getMessage());
					}
				}
				else
				{
					if(choicePcia1.getSelectedIndex() == 0 || choicePcia2.getSelectedIndex() == 0)
					{
						JOptionPane.showMessageDialog(frame, "Seleccione una provincia");
					}
				}
			}
		});
	
		
		
		//BOTON AGM
		JButton botonAGM = new JButton("AGM");
		botonAGM.setFont(new Font("Arial", Font.PLAIN, 18));
		botonAGM.setBounds(390, 459, 215, 66);
		frame.getContentPane().add(botonAGM); 
		botonAGM.setToolTipText("A partir del mapa creado, crea un submapa con un unico camino posible entre sus provincias");
		
		
		//accion AGM
		botonAGM.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				if(mapa.tama�o() > 0 && BFS.esConexo(mapa))
				{
					arbolGeneradorMinimo=AGM.generar(mapa);
					mapa=arbolGeneradorMinimo;
					seAplicoAGM=true;
					actualizarMapaVisual(vistaDeMapa, mapa);
				}
				else
				{
					JOptionPane.showMessageDialog(frame, "Debe haber al menos un arista y el mapa debe tener todas sus provincias conectadas");
				}
			}
		});
		
		
		
		//BOTON PARA REINICIAR
		JButton botonReiniciarMapa = new JButton("Reiniciar");
		botonReiniciarMapa.setFont(new Font("Arial", Font.PLAIN, 15));
		botonReiniciarMapa.setBounds(680, 400, 100, 30);
		frame.getContentPane().add(botonReiniciarMapa);
		botonReiniciarMapa.setToolTipText("Borrar todas las ciudades y conexiones del mapa");
		
		
		//accion reiniciar
		botonReiniciarMapa.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e)
			{
				mapa=new Grafo();
				seAplicoAGM= false;
				actualizarMapaVisual(vistaDeMapa, mapa);
			}
		});
		
		
	}
}

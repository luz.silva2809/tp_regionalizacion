package usuario;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;

@SuppressWarnings("serial")
public class Regionalizacion extends JFrame {

	protected static JMapViewer mapaVisual = null;
	private JPanel contentPane;

	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Regionalizacion frame = new Regionalizacion(mapaVisual);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}


	public Regionalizacion(JMapViewer mapaRegionalizado) 
	{
		mapaVisual=mapaRegionalizado;
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(500, 50, 450, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		mapaVisual.setBounds(50, 50, 350, 500);
		Coordinate coordenadasCentroArgentina = new Coordinate(-41, -65);
		mapaVisual.setDisplayPosition(coordenadasCentroArgentina, 4);
		
		contentPane.add(mapaVisual);
		contentPane.setLayout(null);	
	}
}

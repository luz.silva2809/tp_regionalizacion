package negocio;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public class AGM
{
	private static Set<String>verticesEnArbol;
	private static Map<String, String> aristasEnArbol;
	
	
	
	
	//genera desde un grafo un AGM y lo retorna
	public static Grafo generar(Grafo grafoOriginal)
	{
			Grafo arbol=new Grafo();

			verticesEnArbol= new TreeSet<String>();
			verticesEnArbol.add(unaCiudad(grafoOriginal));

			aristasEnArbol= new TreeMap<String, String>();

			int i=1;

			while(i <= grafoOriginal.tama�o()-1)
			{
				Integer menorPeso= Integer.MAX_VALUE;
				String ciudadOrigen="";
				String ciudadDestino="";

				for (String ciudadVisitada: verticesEnArbol) 
				{
					for (String ciudadVecina : grafoOriginal.vecinosDe(ciudadVisitada))
					{
						if(grafoOriginal.getSimilaridad(ciudadVisitada, ciudadVecina) < menorPeso)
						{
							if(!verticesEnArbol.contains(ciudadVecina))
							{
								menorPeso=grafoOriginal.getSimilaridad(ciudadVisitada, ciudadVecina);
								ciudadOrigen=ciudadVisitada;
								ciudadDestino=ciudadVecina;
							}
						}
					}
				}

				if(ciudadOrigen.equals("") || ciudadDestino.equals(""))
				{
					throw new IllegalArgumentException("El grafo no es conexo");
				}
				aristasEnArbol.put(ciudadOrigen, ciudadDestino);
				verticesEnArbol.add(ciudadDestino);

				arbol.agregarVertice(ciudadOrigen);
				arbol.agregarVertice(ciudadDestino);
				arbol.agregarArista(ciudadOrigen, ciudadDestino, menorPeso);
				i++;
			}

			return arbol;
	}
	
	
	
	
	//elige un vertice desde el cual se genera el AGM
		private static String unaCiudad(Grafo grafoOriginal)
		{
			return grafoOriginal.getCiudades().iterator().next();
		}

}

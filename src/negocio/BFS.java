package negocio;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class BFS 
{
	private static Set<String> marcados;
	private static ArrayList<String> pendientes;
	
	
	//verifica si un garfo es conexo
	public static boolean esConexo(Grafo grafo)
	{
		if(grafo == null)
		{
			throw new NullPointerException("el grafo es nulo");
		}
		if(grafo.tama�o() == 0)
		{
			return true;
		}
		return alcanzables(grafo, grafo.getCiudades().iterator().next()).size() == grafo.tama�o();
	}
	
	
	//devuelve un conjunto con los vertices a los que puede llegar desde un vertice de origen
	public static Set<String> alcanzables(Grafo grafo, String ciudad) 
	{
		inicializar(grafo, ciudad);
		while (pendientes.size() > 0)
		{
			String ciudadPendiente=pendientes.get(0);
			marcados.add(ciudadPendiente);
			agregarVecinosPendientes(grafo, ciudadPendiente);
			pendientes.remove(0);
		}
		return marcados;
	}
	

	
	//agrega en la lista de pendientes los vecinos de un vertice que aun no fueron alcanzados
	private static void agregarVecinosPendientes(Grafo grafo, String ciudad) 
	{
		for (String vecino : grafo.vecinosDe(ciudad)) 
		{
			if(marcados.contains(vecino) == false && pendientes.contains(vecino) == false)
			{
				pendientes.add(vecino);
			}
		}
	}
	
	
	
	//inicializa el recorrido por el grafo
	static private void inicializar(Grafo g, String ciudad)
	{
		pendientes = new ArrayList<String>();
		pendientes.add(ciudad);
		marcados = new HashSet<String>();
	}
	
}

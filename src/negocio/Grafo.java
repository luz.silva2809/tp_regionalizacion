package negocio;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Grafo {
	
	private Map<String, Map<String, Integer>> mapa;
	
	
	
	public Grafo ()
	{
		this.mapa = new HashMap<String, Map<String, Integer>>();
	}
	
	
	
	// agrega una ciudad al mapa
	public void agregarVertice(String ciudad) 
	{
		if (!existeVertice(ciudad)) 
		{ 
			mapa.put(ciudad, new HashMap<String, Integer>());
		}
	}
	
	
	
	// agrega a una ciudad un vecino (y viceversa) y el peso de su arista
	public void agregarArista(String origen, String destino, Integer similaridad) 
	{
		verificarLoop(origen, destino);
		if (existeVertice(origen) && existeVertice(destino))
		{
			mapa.get(origen).put(destino, similaridad);
			mapa.get(destino).put(origen, similaridad);
		}
	}
	
	
	
	// elimina en la lista de vecinos de origen a destino (y viceversa)
	public void eliminarArista(String origen, String destino) 
	{
		if (existeArista(origen, destino))
		{
			mapa.get(origen).remove(destino);
			mapa.get(destino).remove(origen);
		}
		else
		{
			throw new IllegalArgumentException("La arista entre "+ origen + " y "+ destino+ " no existe");
		}
	}
	
	
	
	// elimina una ciudad del mapa 
	//y a su vez elimina esta ciudad de sus anteriores vecinos
	public void eliminarVertice (String estaCiudad) 
	{
		if (existeVertice(estaCiudad))
		{
			for(String vecino: vecinosDe(estaCiudad))
			{
				eliminarArista(vecino, estaCiudad);
			}
			mapa.remove(estaCiudad);
		}
		else
			throw new NullPointerException("La ciudad que quiere eliminar no existe");
	}
	
	
	
	// verifica si existe la arista entre origen y destino
	public boolean existeArista(String origen, String destino) 
	{
		return existeVertice(origen) && vecinosDe(origen).contains(destino);
	}


	
	// verifica que el mapa contenga a la ciudad
	public boolean existeVertice(String ciudad)
	{
		return getCiudades().contains(ciudad);	
	}
	
	
	
	// devuelve la similaridad entre origen y destino
	public Integer getSimilaridad(String origen, String destino)
	{
		if (existeArista(origen, destino))
		{
			return mapa.get(origen).get(destino);
		}
		throw new IllegalArgumentException("La arista entre "+origen+" y "+destino+" no existe");
	}

	

	// devuelve la cantidad de ciudades del mapa
	public int tama�o() 
	{
		return mapa.size();
	}
	

	
	// devuelve un conjunto con todas las ciudades del mapa
	public Set<String> getCiudades() 
	{
			return mapa.keySet();
	}
	
	

	// devuelve un conjunto con todas las ciudades vecinas de estaCiudad
	public Set<String> vecinosDe(String estaCiudad) 
	{
		if (existeVertice(estaCiudad))
			return mapa.get(estaCiudad).keySet();
		
		throw new IllegalArgumentException("La ciudad "+estaCiudad+" no existe");
	}

	 
	
	//elimina las n-1 (cantidad de regiones) aristas mas pesadas 
	public void regionalizar(int cantidadDeRegiones)
	{
		if(cantidadDeRegiones > tama�o())
		{
			throw new IllegalArgumentException
			("La cantidad de regiones debe ser menor o igual a la cantidad de ciudades que es: "+tama�o());
		}
		
		for(int i=1; i < cantidadDeRegiones; i++)
		{
			eliminarAristaMasPesada();
		}
	}

	

	// elimina un arista, en particular la mas pesada del grafo
	private void eliminarAristaMasPesada() 
	{
		String ciudadOrigen="";
		String ciudadDestino="";
		int mayorPeso=-1;
		
		for(String ciudad: getCiudades())
		{
			for(String vecino: vecinosDe(ciudad))
			{
				if(getSimilaridad(ciudad, vecino) > mayorPeso)
				{
					ciudadOrigen=ciudad;
					ciudadDestino=vecino;
					mayorPeso=getSimilaridad(ciudad, vecino);
				}
			}
		}
		
		eliminarArista(ciudadOrigen, ciudadDestino);
	}

	

	// hace una copia exacta del grafo original
	public Grafo clonar()
	{
		Grafo nuevoGrafo=new Grafo();
		
		for(String ciudad: getCiudades())
		{
			nuevoGrafo.agregarVertice(ciudad);
			for(String ciudad2: vecinosDe(ciudad))
			{
				nuevoGrafo.agregarArista(ciudad, ciudad2, getSimilaridad(ciudad,ciudad2));
			}
		}
		
		return nuevoGrafo;
	}
	
	
	
	// verifica loop entre 2 ciudades
	private void verificarLoop(String ciudad1, String ciudad2) 
		{
			if (ciudad1.equals(ciudad2))
				throw new IllegalArgumentException
				("La ciudad de origen no puede ser igual a la ciudad de destino");
		}
}
